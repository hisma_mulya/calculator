//
//  multiplyTests.m
//  calculatorTests
//
//  Created by Hisma Mulya S on 2/22/18.
//  Copyright © 2018 BTPN. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "CalculatorManager.h"

@interface multiplyTests : XCTestCase

@property(nonatomic, strong) CalculatorManager *manager;

@end

@implementation multiplyTests

- (void)setUp {
    [super setUp];
    self.manager = [CalculatorManager new];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    self.manager = nil;
}

- (void)testShouldBeAbleToMultiplyDecimalNumberWithoutFraction {
    NSDecimalNumber *result = [self.manager multiply:[NSDecimalNumber decimalNumberWithString:@"2"]
                                              with:[NSDecimalNumber decimalNumberWithString:@"2"]];

    XCTAssertEqualObjects(result, [NSDecimalNumber decimalNumberWithString:@"4"]);
}

- (void)testShouldBeAbleToDivideDecimalNumberWithFraction {
    NSDecimalNumber *result = [self.manager multiply:[NSDecimalNumber decimalNumberWithString:@"2"]
                                              with:[NSDecimalNumber decimalNumberWithString:@"2.5"]];

    XCTAssertEqualObjects(result, [NSDecimalNumber decimalNumberWithString:@"5"]);
}

- (void)testShouldThrowExceptionIfThereIsNilOperand {
    XCTAssertThrows([self.manager multiply:[NSDecimalNumber decimalNumberWithString:nil]
                                    with:[NSDecimalNumber decimalNumberWithString:@"2.5"]]);
    XCTAssertThrows([self.manager multiply:[NSDecimalNumber decimalNumberWithString:@"234"]
                                    with:[NSDecimalNumber decimalNumberWithString:nil]]);
}

- (void)testThrowExceptionIfInsertedEmptyString {
    XCTAssertThrows([self.manager multiply:[NSDecimalNumber decimalNumberWithString:@""]
                                      with:[NSDecimalNumber decimalNumberWithString:@"2.5"]]);
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end

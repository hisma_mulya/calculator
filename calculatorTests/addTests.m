//
//  addTests.m
//  calculatorTests
//
//  Created by Hisma Mulya S on 2/22/18.
//  Copyright © 2018 BTPN. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "CalculatorManager.h"

@interface addTests : XCTestCase

@property(nonatomic, strong) CalculatorManager *manager;

@end

@implementation addTests

- (void)setUp {
    [super setUp];
    self.manager = [CalculatorManager new];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    self.manager = nil;
}

- (void)testShouldBeAbleToAddDecimalNumberWithoutFraction {
    NSDecimalNumber *result = [self.manager add:[NSDecimalNumber decimalNumberWithString:@"2"]
                                           with:[NSDecimalNumber decimalNumberWithString:@"2"]];

    XCTAssertEqualObjects(result, [NSDecimalNumber decimalNumberWithString:@"4"]);
}

- (void)testShouldBeAbleToAddDecimalNumberWithFraction {
    NSDecimalNumber *result = [self.manager add:[NSDecimalNumber decimalNumberWithString:@"2"]
                                           with:[NSDecimalNumber decimalNumberWithString:@"2.5"]];

    XCTAssertEqualObjects(result, [NSDecimalNumber decimalNumberWithString:@"4.50"]);
}

- (void)testShouldThrowExceptionIfThereIsNilOperand {
    XCTAssertThrows([self.manager add:[NSDecimalNumber decimalNumberWithString:nil]
                                 with:[NSDecimalNumber decimalNumberWithString:@"2.5"]]);
    XCTAssertThrows([self.manager add:[NSDecimalNumber decimalNumberWithString:@"234"]
                                 with:[NSDecimalNumber decimalNumberWithString:nil]]);
}

- (void)testThrowExceptionIfInsertedEmptyString {
    XCTAssertThrows([self.manager add:[NSDecimalNumber decimalNumberWithString:@""]
                                 with:[NSDecimalNumber decimalNumberWithString:@"2.5"]]);
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
        [self.manager add:[NSDecimalNumber decimalNumberWithString:@"2"]
                     with:[NSDecimalNumber decimalNumberWithString:@"2"]];
    }];
}

@end

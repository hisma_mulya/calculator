//
//  CalculatorManager.h
//  calculator
//
//  Created by Hisma Mulya S on 2/22/18.
//  Copyright © 2018 BTPN. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CalculatorManager : NSObject

- (NSDecimalNumber *)add:(NSDecimalNumber *)firstNumber with:(NSDecimalNumber *)secondNumber;
- (NSDecimalNumber *)subtract:(NSDecimalNumber *)firstNumber with:(NSDecimalNumber *)secondNumber;
- (NSDecimalNumber *)divide:(NSDecimalNumber *)firstNumber with:(NSDecimalNumber *)secondNumber;
- (NSDecimalNumber *)multiply:(NSDecimalNumber *)firstNumber with:(NSDecimalNumber *)secondNumber;

@end

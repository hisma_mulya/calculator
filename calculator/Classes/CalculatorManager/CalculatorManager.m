//
//  CalculatorManager.m
//  calculator
//
//  Created by Hisma Mulya S on 2/22/18.
//  Copyright © 2018 BTPN. All rights reserved.
//

#import "CalculatorManager.h"

@implementation CalculatorManager

- (NSDecimalNumber *)add:(NSDecimalNumber *)firstNumber
                    with:(NSDecimalNumber *)secondNumber
{
    NSParameterAssert(firstNumber);
    NSParameterAssert(secondNumber);

    return [firstNumber decimalNumberByAdding:secondNumber];
}

- (NSDecimalNumber *)subtract:(NSDecimalNumber *)firstNumber
                         with:(NSDecimalNumber *)secondNumber
{
    NSParameterAssert(firstNumber);
    NSParameterAssert(secondNumber);

    return [firstNumber decimalNumberBySubtracting:secondNumber];
}

- (NSDecimalNumber *)divide:(NSDecimalNumber *)firstNumber
                       with:(NSDecimalNumber *)secondNumber
{
    NSParameterAssert(firstNumber);
    NSParameterAssert(secondNumber);

    return [firstNumber decimalNumberByDividingBy:secondNumber];
}

- (NSDecimalNumber *)multiply:(NSDecimalNumber *)firstNumber
                         with:(NSDecimalNumber *)secondNumber
{
    NSParameterAssert(firstNumber);
    NSParameterAssert(secondNumber);

    return [firstNumber decimalNumberByMultiplyingBy:secondNumber];
}



@end

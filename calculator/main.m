//
//  main.m
//  calculator
//
//  Created by Hisma Mulya S on 2/22/18.
//  Copyright © 2018 BTPN. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
